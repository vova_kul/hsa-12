# HSA-12 Homework project for balancing

This an HSA-12 Homework project to show geo balancing.

## Geo balancing

#### Run
```
docker-compose up
ngrok http 80
```

#### Results

Ukraine (no VPN)

![Ukraine](./screens/Ukraine.png?raw=true "Ukraine")

UK (VPN)

![UK](./screens/UK.png?raw=true "UK")

US (VPN)

![UK](./screens/US.png?raw=true "US")
